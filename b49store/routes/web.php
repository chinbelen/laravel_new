<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Route::resource('categories', 'CategoryController');
// /categories, /categories/{id}

# separate the resource route into 2 wherein the first one uses the middleware and the second won't
Route::resource('products', 'ProductController', ["except" => ["show", "index"]])->middleware("isAdmin");
	# the routes in this resource except for show and index will use the isAdmin middleware

Route::resource('products', 'ProductController', ['only' => ['show', 'index']]);
	# use the routes show and index only of this resources, without a middleware

Route::resource('statuses', 'StatusController');
Route::resource('orders', 'OrderController');

# Route to empty cart:
Route::delete('cart/empty','CartController@emptyCart');
// Route::get('cart/empty','CartController@emptyCart');

# Route to confirm order after checkout:
Route::get("/cart/confirm", "CartController@confirmOrder");

Route::resource('cart', 'CartController'); #base url /cart
# q: what is post/cart? -> store
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/* CHECK IF THE USER IS AN ADMIN

create
store
edit
update
destroy

NO CHECKS IF ADMIN OR NOT

show (product details)
index (catalog)

*/