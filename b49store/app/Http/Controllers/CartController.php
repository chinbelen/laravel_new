<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use Session;
use App\Product; //allows us to use the Product model
use Auth; //allows us to call the User Session

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(session()->get('cart'));
        // Session::forget("cart"); //to delete
        //use session cart to get the details for the item
        $details_of_items_in_cart = [];
        $total = 0;
        if(Session::exists("cart") || Session::get("cart") !== null){
            foreach (Session::get("cart") as $item_id => $quantity) {
        //because session cart has keys ($item_id) and values ($quantity)
        //     //1. Find the product
            $product = Product::find($item_id);
        //     //2. Get the details needed (add properties not in the original item/catalog)
            $product->quantity = $quantity;
            $product->subtotal = $product->price * $quantity;
        //         // note: these properties are not part of the product stored in the database, they are only for $product
            $total += $product->subtotal;
        //     // 3. Push to array containing the details
            array_push($details_of_items_in_cart, $product);
            }
        }
          // dd($details_of_items_in_cart);
        // send the array to the view
        return view("products/cart", compact("details_of_items_in_cart" , "total"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request); /*request->parameters->item_id & quantity*/
        // $cart[$request->item_id] = $request->quantity;  /* $cart[key/index] = value; 
        /*  Ways to create an array:
        $cart = array(values);
        $cart = [value1, value2, etc];
        $cart[key/index] = value;
            What happens: When an item is added to cart, it will assign the quantity to index number equal to the item_id, "item_id->quantity" 
            To check: dd($cart)  */
        // Session::put("cart", $cart); -> adds a session variable called cart with the content from $cart

         /*
         This is only halfway done because Session::put overwrites the original content. Find a way to prevent the overwriting (i.e revamp the entire logic)
            1. Check if we already have a cart
                a. if there is no cart, create a new one
                b. if there is already one, call the cart and update the content
            2. save the updated cart in the session */

        // $cart = []; //empty cart
        // if(Session::exists("cart")){ //if there is a cart in our session, we call the cart
        //     $cart = $request->session()->get('cart');
        // }
        // $cart[$request->item_id] = $request->quantity; //update the cart
        // Session::put("cart", $cart); //push it back to the session cart
        // Session::flash("message", $request->quantity . " items added to cart");
        //Syntax: put into $cart into a session variable called "cart"
        // dd(Session::get("cart")); //to check cart
        // return redirect("/products"); //we simple return to the catalog page after adding to cart

            $cart = [];
            if ($request->session()->has('cart')){
                $cart = $request->session()->get('cart');
            }
            $cart[$request->item_id] = $request->quantity;
            $request->session()->put('cart', $cart);
            Session::flash("message", $request->quantity . " items added to cart");
            return redirect("/products");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cart = Session::get('cart');
        $cart[$id] = $request->newqty;
        Session::put('cart', $cart);
        return redirect("/cart");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // return "item with id: $id to be removed from cart"
        Session::forget("cart.".$id);//to remove specific key //unset($_SESSION['cart']['id']) 
        return redirect("/cart");

    }

    public function emptyCart()
    {
        if (Session::exists("cart")){
            Session::forget("cart");
        }
        return redirect("/cart");
    }

    public function confirmOrder()
    {
        if (Auth::check()){
            $details_of_items_in_cart = [];
            $total = 0;
                if(Session::exists("cart") || Session::get("cart") !== null){
                    foreach (Session::get("cart") as $item_id => $quantity) {
                        $product = Product::find($item_id);
                        $product->quantity = $quantity;
                        $product->subtotal = $product->price * $quantity;
                        $total += $product->subtotal;
                        array_push($details_of_items_in_cart, $product);
                    }
                    return view("orders.confirm", compact("details_of_items_in_cart" , "total"));
                }
            } else {
                return redirect('/login');
            }
        }
    }