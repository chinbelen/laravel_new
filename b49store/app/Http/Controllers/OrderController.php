<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\Status;
use Session; //to get the cart

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //to view the orders as users
        if(Auth::check()){ //only shows the orders for those who are logged in
            if(Auth::user()->isAdmin){
                $orders = Order::all(); //gets the list of ALL orders
            } else {
                $orders = Order::where("user_id", Auth::user()->id)->get(); //gets the orders belonging to the user
                //note: find() was not used because it is only used for primary keys
                /*$orders = Order::select("refNo", "status_id", "total")
                        ->where("user_id", Auth::user()->id)
                        ->get();*/
            }
            // dd($orders);
            return view("orders.orderlist", compact("orders"));
        } else {
            return redirect("/login");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 1. Create a new order with total = 0;
        $refNo = "B49".time();
        $new_order = new Order;
        $new_order->refNo = $refNo;
        $new_order->user_id = Auth::user()->id;
        $new_order->status_id = 1; //always pending (default)
        $new_order->total = 0;
        // dd($new_order); //test this out, check if the user_id is correct and if the refNo is updated
        $new_order->save(); //save the order item
        // 2. Attach the items or the products to the order -> write to the products_orders table
        $total=0;
        foreach (Session::get("cart") as $item_id => $quantity) {
            $product = Product::find($item_id); //to get the price property
            $subtotal = $product->price * $quantity;
            //checklist to attach: order_id ($order), item_id ($item_id), quantity ($quantity), subtotal ($subtotal) 
            //to populate the products_orders table
            $new_order->products()->attach($item_id, ["quantity" => $quantity, "subtotal" => $subtotal]);
                //"for this order, get the pivot table (products_orders) and create a new row with its order_id, item_id and the quantity and subtotal fields"
                //the second paramater of attach()/associative array contains all the pivot columns
            //update total
            $total += $subtotal;
        }
        // 3. Update the new order's total
        $new_order->total = $total;
        $new_order->save();
        Session::forget("cart"); //to delete cart content after checkout
        // return "Order Successfully Created";

        return view("orders.result", compact("refNo"));
        // Create a view that contains a line saying "Your order <refNo> has been successfully created" and a button that bring it back to the catalog
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $order->status_id = 2; //Update status to Completed
        $order->save();
        return redirect("/orders");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->status_id = 3; //Update status to Cancelled
        $order->save();
        return redirect("/orders");
    }
}
