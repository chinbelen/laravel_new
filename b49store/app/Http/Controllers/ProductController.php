<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category; /*import the model needed for create function*/
use Illuminate\Http\Request;
use Session;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all the products from the database
        $products = Product::all();
        // dd($products);
        return view('products.catalog', compact('products')); //catalog.blade.php in views/products 
        //compact-calls all products that correspond to $products
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // retrieve all categories via the all method of the Category model
        $categories = Category::all();
        return view('products.create', compact('categories'));/*compact method converts it into an array, so we can use foreach*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->category_id = $request->category;

        //manage the image upload
        $image = $request->file('image');
        //actual file name when stored in project directory
        //date & time concatenated with a . concatenated with the file extension
        $image_name = time().".".$image->getClientOriginalExtension();
        //specify the subdirectory within the project's "public" folder where the image file will be saved
        $destination = "images/";
        //perform the actual saving of the image file by passing in the target directory and the desired file name to the move() method of the $image variable
        $image->move($destination, $image_name);
        // set the relative link pointing to the location of the image asset as the value for the product's img_path property
        $product->img_path =  $destination.$image_name;
        //save this product in the database
        $product->save();
        $request->session()->flash('add_product', '"'. $product->name .'" successfully added!');
        return redirect("/products/create"); //go to the route
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //Product $product finds the specific product via the ID of the product, the id comes from the url that was specified in the route file
        return view('products.product', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //use this function to have a view showing the details of the item in a form (edit.blade.php)
        // Create the form in edit.blade.php
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        /*no need to use find(id), since it is already $product in the function*/
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->category_id = $request->category;

        if($request->file('image') == ""){ //if there is no image reuploaded
            $product->img_path = $product->img_path; //will retain the current image
        } else {
        $image = $request->file('image');
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $image_name);
        $product->img_path = $destination.$image_name;
        }

        /* OR

        if($request->file('image') != ""){ //if there is an image uploaded
            $image = $request->file('image');
            $image_name = time().".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $product->img_path = $destination.$image_name;
        }
        */

        $product->save();
        $request->session()->flash('edit_product', 'Changes saved for "'. $product->name .'" !');
        return redirect('/products/'.$product->id); //will redirect to the particular product
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // dd($product);
        $product->delete();
        Session::flash('delete_product', 'Product "'.$product->name.'" successfully deleted');
        return redirect('/products');
    }
}
