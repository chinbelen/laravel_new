<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function products(){
    	/*if we call the products() method of a category object, we would be able to call all the products for that particular category and we can also pull their respective properties*/
    	// from parent to child is always plural
    	return $this->hasMany('\App\Product');
    }
}
