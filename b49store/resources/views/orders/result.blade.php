@extends('layouts.app')

@section('content')
	<div class="col-lg-6 offset-lg-3 text-center">
		<h4 class="m-5">Your order with Reference No. {{$refNo}} has been successfully created!</h4>
		<a href="/products" class="btn btn-outline-success">Go back to Products</a>
	</div>
@endsection