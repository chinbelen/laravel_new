@extends('layouts.app')

@section('content')
	<div class="text-center mt-5">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<h2>Orders</h2>
				<table class="table table-striped">
					<thead>
						<th>Reference No.</th>
						<th>Username</th>
						<th>Total</th>
						<th>Status</th>
						<th>Details</th>
						@if(Auth::user()->isAdmin)
							<th>Actions:</th>
						@endif
					</thead>
					<tbody>
						@foreach($orders as $order)
							<tr>
								<td>{{$order->refNo}}</td>
								<td>{{$order->user->name}}</td>
								<td>{{$order->total}}</td>
								<td>{{$order->status->name}}</td>
								<td class="text-left">
									@foreach($order->products as $product)
									{{-- $order->products used the pivot table products_orders to look at the details of ALL the products linked to the order --}}
										<p>{{$product->name}}, ({{$product->pivot->quantity}})</p>	
									{{-- pivot refers to the columns associated to the product in pivot table --}}
									@endforeach
								</td>
								@if(Auth::user()->isAdmin && $order->status_id == 1)
									<td>
										<form action="/orders/{{$order->id}}" method="POST">
											@csrf
											{{method_field("PATCH")}}
											<button type="submit" class="btn btn-outline-success">Complete</button>
										</form>
									</td>
									<td>
										<form action="/orders/{{$order->id}}" method="POST">
											@csrf
											{{method_field("DELETE")}}
											<button type="submit" class="btn btn-outline-danger">Cancel</button>
										</form>
									</td>				
								@endif
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection