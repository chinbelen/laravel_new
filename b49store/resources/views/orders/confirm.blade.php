@extends('layouts.app')

@section('content')
	<div class="text-center mt-5">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<h2>Order Summary</h2>
				<table class="table table-striped">
					<thead>
						<th>Name: </th>
						<th>Description: </th>
						<th>Price: </th>
						<th>Quantity: </th>
						<th>Subtotal: </th>
					</thead>
					<tbody>
						{{-- use the foreach loop to display the items in their corresponding cells --}}
					@if(!empty($details_of_items_in_cart))
						@foreach($details_of_items_in_cart as $indiv_product)
							<tr>
								<td>{{$indiv_product->name}}</td>
								<td>{{$indiv_product->description}}</td>
								<td>{{$indiv_product->price}}</td>
								<td>{{$indiv_product->id}}</td>
								<td>₱ {{$indiv_product->subtotal}}</td>
							</tr>
						@endforeach
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td><strong>Total:</strong></td>
							<td>₱ {{$total}}</td>
						</tr>
					@else
						<tr>
							<td colspan="6">No orders to display</td>
						</tr>
					@endif
					</tbody>
				</table>
				
				
				<form action="/orders" method="POST">
					@csrf
					<button type="submit" class="btn btn-success">Confirm Order</button>
				</form>
				<a href="/cart" class="btn btn-info mt-2">Back to Cart</a>
			</div>		
		</div>
	</div>
@endsection