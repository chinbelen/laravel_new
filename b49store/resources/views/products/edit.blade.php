@extends('layouts.app')

@section('content')
	<div class="row mt-5">
		<div class="col-lg-8 offset-lg-2">
			<form method="POST" action="/products/{{$product->id}}" enctype="multipart/form-data">
			@csrf
			{{method_field('PUT')}}
				<div class="form-group">
					<label for="name">Product Name: </label>
					<input type="text" name="name" value="{{$product->name}}" class="form-control">
				</div>
				<div class="form-group">
					<label for="description">Description: </label>
					<textarea name="description" class="form-control">{{$product->description}}</textarea>
				</div>
				<div class="form-group">
					<img src="{{asset($product->img_path)}}" style="height: 100px">
					<br>
					<label for="image">Upload Image: </label>
					<input type="file" name="image">
				</div>
				<div class="form-group">
					<label for="price">Price in Php: </label>
					<input type="number" name="price" value="{{$product->price}}">
				</div>
				<div class="form-group">
					<select name="category">
						@foreach(App\Category::all() as $category)
							<option value="{{$category->id}}" {{$product->category_id == $category->id ? 'selected' : ""}}>
							{{-- This is a ternarny operator, it is similar to an if-else statement but it is a single line and only outputs values --}}
							{{-- condition ? value if true : value if false --}}
								{{$category->name}}
							</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-success">Save Changes</button>
			</form>
		</div>
	</div>
@endsection
