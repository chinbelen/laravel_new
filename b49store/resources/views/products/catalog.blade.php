@extends('layouts.app')

@section('content')
	<div class="text-center mt-5">
		<h3>Products</h3>
			{{-- add a button for all --}}
		<a href="/products" class="btn btn-primary">All</a>
			{{-- add buttons for each category --}}
		@foreach (App\Category::all() as $indiv_category)
			<a href="/categories/{{$indiv_category->id}}" class="btn btn-primary">{{$indiv_category->name}}</a>
			{{-- This uses the categories route (/categories) and needs the show() method of the CategoryController--}}
		@endforeach
		<div class="row">
			@if(Session::has("message"))
				<h4>{{Session::get("message")}}</h4>
			@endif
		</div>
		<div class="row mb-2">
			@if(Session::has("delete_product"))
				<h4>{{Session::get("delete_product")}}</h4>
			@endif
	</div>

	<div class="row offset-1">	
	@foreach ($products as $indiv_product)
		<div class="card m-3" style="max-width: 20rem;">
  			<img src="{{asset($indiv_product->img_path)}}" class="card-img-top" height="300px" alt="...">
  			<div class="card-body">
    			<h5 class="card-title" style="font-weight: bold;">{{$indiv_product->name}}</h5>
   		 		<p class="card-text">{{$indiv_product->description}}</p>
   		 		<p class="card-text"><small>Php {{$indiv_product->price}}</small></p>
   		 		<a href="/products/{{$indiv_product->id}}" class="btn btn-info">View Details</a>
  			</div>
  			
  			@if(!Auth::check() || !Auth::user()->isAdmin)
  			<div class="card-footer">
  				<form action="/cart" method="POST">{{-- $item_id -> quantity --}}
  				@csrf
  					<input type="hidden" name="item_id" value="{{$indiv_product->id}}"> 
  					<input type="number" name="quantity" class="form-control" min="1" value="1">
  					<button type="submit"class="btn btn-success mt-1">Add To Cart</button>
  				</form>
  			</div>
  			@endif

		</div>
	@endforeach
	</div>

@endsection