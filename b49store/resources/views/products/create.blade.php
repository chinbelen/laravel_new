@extends('layouts.app')

@section ('content')
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<form method="POST" action="/categories">
				@csrf
				<div class="form-group">
					<label for="name">Name: </label>
					<input type="text" name="name" id="name" class="form-control">
				</div>
			<button type="submit" class="btn btn-success">Add new category</button>	
			</form>
			<div class="row">
				@if(Session::has("add_category"))
					<h4>{{Session::get("add_category")}}</h4>
				@endif
			</div>
		</div>
	</div>
	{{-- end of row for categories --}}
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<form method="POST" action="/products" enctype="multipart/form-data">
				@csrf
				<div class="form-group">
					<label for="name">Name: </label>
					<input type="text" name="name" id="name" class="form-control">
				</div>
				<div class="form-group">
					<label for="description">Description: </label>
					<textarea name="description" id="description" class="form-control"></textarea>
				</div>
				<div class="form-group">
					<label for="price">Price: </label>
					<input type="number" name="price" id="price" step=0.01 min=0 class="form-control">
				</div>
				<div class="form-group">
					<label for="image">Upload Image: </label>
					<input type="file" name="image" id="image">
				</div>
				<div class="form-group">
					<select name="category">
						@foreach ($categories as $category)
							<option value="{{$category->id}}">
								{{$category->name}}
							</option>
						@endforeach
					</select>
				</div>
				<button type="submit" class="btn btn-success">Add new product</button>
			</form>
			<div class="row">
				@if(Session::has("add_product"))
					<h4>{{Session::get("add_product")}}</h4>
				@endif
			</div>
		</div>
	</div>
	{{-- end of row for products --}}
@endsection