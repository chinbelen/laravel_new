@extends('layouts.app')

@section('content')
	<div class="text-center mt-5">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<h2>My Cart</h2>
				<table class="table table-striped">
					<thead>
						<th>Name: </th>
						<th>Description: </th>
						<th>Price: </th>
						<th>Quantity: </th>
						<th>Subtotal: </th>
						<th></th>
					</thead>
					<tbody>
						{{-- use the foreach loop to display the items in their corresponding cells --}}
					@if(!empty($details_of_items_in_cart))
						@foreach($details_of_items_in_cart as $indiv_product)
							<tr>
								<td>{{$indiv_product->name}}</td>
								<td>{{$indiv_product->description}}</td>
								<td>{{$indiv_product->price}}</td>
								<td>
									<form action="/cart/{{$indiv_product->id}}" method="POST">
									@csrf
									{{method_field('PATCH')}}
									<input type="number" name="newqty" value={{$indiv_product->quantity}} min=1>
									<button class="btn btn-info">Update</button>
									</form>
								</td>
								<td>₱ {{$indiv_product->subtotal}}</td>
								<td>
								<form action="/cart/{{$indiv_product->id}}" method="POST">
									@csrf
									{{method_field('DELETE')}}
									<button class="btn btn-danger"><i class="fas fa-trash"></i></button>
								</form>
								</td>
							</tr>
						@endforeach
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td><strong>Total:</strong></td>
							<td>₱ {{$total}}</td>
							<td></td>
						</tr>
					@else
						<tr>
							<td colspan="6">No items to display</td>
						</tr>
					@endif
					</tbody>
				</table>
				
				<a href="/cart/confirm" class="btn btn-success">Checkout</a>
				<a href="/products" class="btn btn-info">Add more items</a>
				{{-- <a href="/cart/empty" class="btn btn-danger">Empty Cart</a> --}}
				<form action="/cart/empty" method="POST">
					@csrf
					{{method_field('DELETE')}}
					<button type="submit" class="btn btn-danger">Empty Cart</button>
				</form>

			</div>		
		</div>
	</div>
	<script src="https://kit.fontawesome.com/5b189c5a91.js" crossorigin="anonymous"></script>
@endsection