@extends('layouts.app')

@section('content')
{{-- put all the details of the item including the image --}}

<div class="col-lg-12 my-5 text-center">
	<div class="row mb-5">
		@if(Session::has("edit_product"))
			<h4>{{Session::get("edit_product")}}</h4>
		@endif
	</div>
    <h5 style="font-weight: bold;">{{$product->name}}</h5>
	<img src="{{asset($product->img_path)}}" class="card-img-top my-4" style="width: 800px; border-radius:15px;" alt="...">
  	<p><em>{{$product->description}}</em></p>
   	<p>Php {{$product->price}}</p>
{{-- Show the category name of the product
	Hint 1: Make sure there is a relationship FROM Product to Category
	Hint 2: treat the category of the product as its property --}}
	
   	<p>Category: {{$product->category->name}}</p>
   	{{-- $product->category pulls ALL of the properties of the category --}}
{{-- add two buttons that would allow editing and deleting of the item --}}
	{{-- @auth --}}
	@if(Auth::check() && Auth::user()->isAdmin) {{-- user is logged in AND as an admin --}}
	<a href="/products/{{$product->id}}/edit" class="btn btn-primary">Edit Product</a>
	<form action="/products/{{$product->id}}" method="POST">
		@csrf
		{{method_field('DELETE')}}
		{{-- POST % GET are the only HTTP verbs supported by method, so we override it to become DELETE--}}
		<button type="submit" class="btn btn-danger">Delete Product</button>
	</form>
	@endif
	{{-- @endauth --}}
</div>
@endsection