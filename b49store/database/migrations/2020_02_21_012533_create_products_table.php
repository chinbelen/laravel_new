<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('description'); /*data type text -> preferred for column expecting many characters*/
            $table->decimal('price', 10, 2); /*10 -> max no. of digits including decimal place, 2 -> min no.*/
            $table->string('img_path');/*image is stored inside public folder*/
            $table->boolean('isActive')->default(true);/*for toggling product*/
            $table->unsignedBigInteger('category_id'); /*foreign key*/
            $table->timestamps();
            //link the product table to the categories table via its foreign key
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
